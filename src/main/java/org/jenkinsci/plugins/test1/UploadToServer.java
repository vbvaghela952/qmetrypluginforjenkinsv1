package org.jenkinsci.plugins.test1;


import java.io.File;
import java.io.FileInputStream;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;



import org.apache.http.HttpEntity;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;

import org.apache.http.impl.client.HttpClients;


public class UploadToServer {
	
	String charset = "UTF-8";
	
	public void uploadToTheServer(String apikeyserver, String jiraurlserver,
    		String password, String testrunnameserver, String labelsserver, String sprintserver, String versionserver, 
    		String componentserver, String username, String fileserver, String selectionserver, String platformserver, String commentserver) throws IOException{
		
					String toEncode=username+":"+password;
			    	byte[] encodedBytes = Base64.getEncoder().encode(toEncode.getBytes());
			    	String encodedString= new String(encodedBytes);
			    	String basicAuth = "Basic " + encodedString;
			    	System.out.println("00000000000000000000000000000000000000000"+basicAuth+"-------------------");
			    	
			    	
		
			    	CloseableHttpClient httpClient = HttpClients.createDefault();
			    	HttpPost uploadFile = new HttpPost(jiraurlserver);
			    	uploadFile.addHeader("Authorization", basicAuth);
			    	
			    	MultipartEntityBuilder builder = MultipartEntityBuilder.create();
			    	builder.addTextBody("apiKey", apikeyserver, ContentType.TEXT_PLAIN);
			    	builder.addTextBody("testRunName", testrunnameserver, ContentType.TEXT_PLAIN);
			    	builder.addTextBody("format", selectionserver, ContentType.TEXT_PLAIN);
			    	
			    	if(platformserver != null && !platformserver.isEmpty())
			    		builder.addTextBody("platform", platformserver, ContentType.TEXT_PLAIN);
			    	if(labelsserver != null && !labelsserver.isEmpty())
			    		builder.addTextBody("labels", labelsserver, ContentType.TEXT_PLAIN);
			    	if(versionserver != null && !versionserver.isEmpty())
			    		builder.addTextBody("versions", versionserver, ContentType.TEXT_PLAIN);
			    	if(componentserver != null && !componentserver.isEmpty())
			    		builder.addTextBody("components", componentserver, ContentType.TEXT_PLAIN);
			    	if(sprintserver != null && !sprintserver.isEmpty())
			    		builder.addTextBody("sprint", sprintserver, ContentType.TEXT_PLAIN);
			    	if(commentserver != null && !commentserver.isEmpty())
			    		builder.addTextBody("comment", commentserver, ContentType.TEXT_PLAIN);

			    	// This attaches the file to the POST:
			    	File f = new File(fileserver);
			    	builder.addBinaryBody(
			    	    "file",
			    	    new FileInputStream(f),
			    	    ContentType.APPLICATION_OCTET_STREAM,
			    	    f.getName()
			    	);

			    	HttpEntity multipart = builder.build();
			    	uploadFile.setEntity(multipart);
			    	CloseableHttpResponse response = httpClient.execute(uploadFile);
			    	//HttpEntity responseEntity = response.getEntity();
			    	//Execute and get the response.
			    	System.out.println(response.toString());
			    	httpClient.close();

			    	System.out.println("TAG HEREEEEEEEEE-------------------------------------------------------------------------");
				 		  
				    System.out.println("Username "+ ":" + username);
				    System.out.println("Password "+ ":" + password);
				    System.out.println("Basic AUTH "+ ":" + basicAuth);
				    System.out.println("format "+ ":" + selectionserver);
				    System.out.println("API Key "+ ":" + apikeyserver);
				    System.out.println("file "+ ":" + fileserver);
				    System.out.println("URL "+ ":" + jiraurlserver);			
	}
}
